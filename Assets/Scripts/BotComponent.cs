﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ziggurat {
	public class BotComponent : MonoBehaviour {
		private Rigidbody _rigidbody;
		public BotComponent Target { get; set; }
		public float Mass => _rigidbody.mass;
		[SerializeField]
		private UnitEnvironment _unitEnvironment;
		public BotColors Color { get; private set; }
		private float _maxVelocity = 5f, _attackRange = 4f, _specifiedAttackSpeed = 2f, _attackSpeed;
		public float _maxSpeed = 10f, _fastAttackDamage = 50f, _strongAttackDamage = 90f, 
			probabilityOfMiss = 0.8f, probabilityOfCritical = 0.5f, probabilityOfStrongAttack = 0.5f;
		public float VisibilityDistance = 20f;
		public AnimationType _animationType;
		public float Health = 100f;
		public float CurrentDamage;

		[SerializeField]
		private float WanderCenterDistance = 5f, WanderRadius = 10f, WanderAngle, WanderAngleRange = 50f;

		private UnitParametrs _unitParametrs;

		private void Start() {
			_unitParametrs = GetComponentInParent<UnitParametrs>();
			_rigidbody = GetComponent<Rigidbody>();

			GetParametrs();

			if(_rigidbody == null) return;

			_animationType = AnimationType.Idle;

			_attackSpeed = 0f;

			if(transform.parent != null) {
				var parentName = transform.parent.name;
				if(parentName.Contains("Blue")) Color = BotColors.Blue;
				else if(parentName.Contains("Red")) Color = BotColors.Red;
				else Color = BotColors.Green;
			}

			Target = GameObject.Find("Pool").GetComponent<BotComponent>();
		}

		private void GetParametrs() {
			Health = _unitParametrs.Health;
			_maxSpeed = _unitParametrs.MaxSpeed;
			_fastAttackDamage = _unitParametrs.FastAttackDamage;
			_strongAttackDamage = _unitParametrs.StrongAttackDamage;
			probabilityOfMiss = _unitParametrs.ProbabilityOfMiss;
			probabilityOfCritical = _unitParametrs.ProbabilityOfCritical;
			probabilityOfStrongAttack = _unitParametrs.ProbabilityOfStrongAttack;
		}

		private void Update() {
			TakeDamage(0f);

			_attackSpeed -= Time.deltaTime;

			if(Target == null && _animationType == AnimationType.Idle) {
				_animationType = AnimationType.Move;
				_unitEnvironment.Moving(1f);
			}

			if(_rigidbody == null || Target == null) return;

			var dist = Vector3.Distance(transform.position, Target.transform.position);

			if(dist <= _attackRange && Target.name == "Pool") Target = null;

			if(dist > _attackRange && _animationType == AnimationType.Idle) {
				_animationType = AnimationType.Move;
				_unitEnvironment.Moving(1f);
			}
			else if(dist <= _attackRange && _animationType == AnimationType.Move) {
				_animationType = AnimationType.Idle;
				_unitEnvironment.Moving(0f);
			}
			else if(dist <= _attackRange && _attackSpeed <= 0f && _animationType == AnimationType.Idle) {
				_attackSpeed = _specifiedAttackSpeed;

				var rand = UnityEngine.Random.Range(0f, 1f);

				if(rand <= probabilityOfStrongAttack) {
					_unitEnvironment.StartAnimation("Strong");
					CurrentDamage = _strongAttackDamage;
					_animationType = AnimationType.StrongAttack;
				}
				else {
					_unitEnvironment.StartAnimation("Fast");
					CurrentDamage = _fastAttackDamage;
					_animationType = AnimationType.FastAttack;
				}
			}
		}

		private void FixedUpdate() {
			if(_animationType == AnimationType.Move) {
				OnMove();
			}

			if(Target == null) {
				OnWander();
			}
		}

		private void OnTriggerEnter(Collider other) {
			if(other.name != "PolyartSword") return;

			var enemy = other.gameObject.GetComponentInParent<BotComponent>();

			//if(enemy == null || enemy.Color == Color) return;
			if(this != enemy.Target) return;

			var rand = UnityEngine.Random.Range(0f, 1f);

			if(rand <= enemy.probabilityOfMiss) {
				TakeDamage(0);
			}
			else {
				rand = UnityEngine.Random.Range(0f, 1f);

				if(rand <= probabilityOfCritical) {
					TakeDamage(enemy.CurrentDamage * 2f);
				}
				else {
					TakeDamage(enemy.CurrentDamage);
				}
			}
		}

		public void TakeDamage(float damage) {
			Health -= damage;

			if(Health <= 0f) {
				_unitEnvironment.StartAnimation("Die");
				_animationType = AnimationType.Die;
			}
		}

		public Vector3 GetVelocity(IgnoreAxisType ignore = IgnoreAxisType.None) {
			return UpdateIgnoreAxis(_rigidbody.velocity, ignore);
		}

		public void SetVelocity(Vector3 velocity, IgnoreAxisType ignore = IgnoreAxisType.None) {
			//_unitEnvironment.Moving(Mathf.Abs(velocity.z));

			_rigidbody.velocity = UpdateIgnoreAxis(velocity, ignore);

			if(Target != null) transform.LookAt(Target.transform);
		}

		private Vector3 UpdateIgnoreAxis(Vector3 velocity, IgnoreAxisType ignore) {
			if((ignore & IgnoreAxisType.None) == IgnoreAxisType.None) return velocity;
			else if((ignore & IgnoreAxisType.X) == IgnoreAxisType.X) velocity.x = 0f;
			else if((ignore & IgnoreAxisType.Y) == IgnoreAxisType.Y) velocity.y = 0f;
			else if((ignore & IgnoreAxisType.Z) == IgnoreAxisType.Z) velocity.z = 0f;

			return velocity;
		}

		public void OnMove() {
			if(Target == null) return;

			var desired_velocity = (Target.transform.position - transform.position).normalized * _maxVelocity;

			var steering = desired_velocity - GetVelocity(IgnoreAxisType.Y);
			steering = Vector3.ClampMagnitude(steering, _maxVelocity) / Mass;

			var velocity = Vector3.ClampMagnitude(GetVelocity() + steering, _maxSpeed);

			SetVelocity(velocity, IgnoreAxisType.Y);
		}

		public void OnWander() {
			var center = GetVelocity(IgnoreAxisType.Y).normalized * WanderCenterDistance;

			var displacement = Vector3.zero;
			displacement.x = Mathf.Cos(WanderAngle * Mathf.Deg2Rad);
			displacement.z = Mathf.Sin(WanderAngle * Mathf.Deg2Rad);
			displacement = displacement.normalized * WanderRadius;

			WanderAngle += UnityEngine.Random.Range(-WanderAngleRange, WanderAngleRange);

			var desired_velocity = center + displacement;

			var steering = desired_velocity - GetVelocity(IgnoreAxisType.Y);
			steering = Vector3.ClampMagnitude(steering, _maxVelocity) / Mass;

			var velocity = Vector3.ClampMagnitude(GetVelocity() + steering, _maxSpeed);

			transform.LookAt(velocity);
			SetVelocity(velocity, IgnoreAxisType.Y);
		}
	}
}