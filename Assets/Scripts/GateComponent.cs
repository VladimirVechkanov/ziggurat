﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Ziggurat {
	public class GateComponent : MonoBehaviour {
		public List<BotComponent> Bots { get; private set; } = new List<BotComponent>();
		[SerializeField]
		private GameObject _bot;
		public float Interval { get; set; }
		[SerializeField]
		private UnitParametrs _unitParametrs;
		public int DieCount { get; set; } = 0;

		private void Start() {
			Interval = 0f;
		}

		private void Update() {
			Interval -= Time.deltaTime;
			if(Interval <= 0f) {
				Interval = _unitParametrs.SpawnInterval;

				var bot = Instantiate(_bot, transform);
				bot.transform.position = new Vector3(transform.position.x, 7f, transform.position.z);
				bot.transform.rotation = transform.rotation;

				Bots.Add(bot.GetComponent<BotComponent>());
			}

			var dieBots = Bots.Where(t => t.Health <= 0f).ToList();
			foreach(var bot in dieBots) {
				Bots.Remove(bot);
			}
		}
	}
}