﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ziggurat {
	public class HealthBarComponent : MonoBehaviour {
		private Camera _camera;
		[SerializeField]
		private Slider _slider;
		private BotComponent _botComponent;

		private void Start() {
			_camera = Camera.main;
			_botComponent = GetComponentInParent<BotComponent>();

			_slider.maxValue = _botComponent.Health;
			_slider.value = _botComponent.Health;

			gameObject.SetActive(!UIComponent._healthIsHide);
		}

		private void Update() {
			transform.LookAt(_camera.transform);
			if(_botComponent.Health >= 0f) {
				_slider.value = _botComponent.Health;
			}
			else {
				_slider.value = 0f;
			}
		}
	}
}