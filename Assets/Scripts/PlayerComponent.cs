﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ziggurat {
	public class PlayerComponent : MonoBehaviour {
		[SerializeField, Tooltip("Regular speed")]
		private float mainSpeed = 100.0f;
		[SerializeField, Tooltip("Multiplied by how long shift is held.  Basically running")]
		private float shiftAdd = 250.0f;
		[SerializeField, Tooltip("Maximum speed when holdin gshift")]
		private float maxShift = 1000.0f;
		[SerializeField, Tooltip("How sensitive it with mouse")]
		private float camSens = 1f;
		private float totalRun = 1.0f;
		private float X, Y, Z, eulerX = 0, eulerY = 0;

		private void FixedUpdate() {
			if(Input.GetKey(KeyCode.Mouse1)) {
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Locked;
				X = Input.GetAxis("Mouse X") * camSens * Time.deltaTime;
				Y = -Input.GetAxis("Mouse Y") * camSens * Time.deltaTime;
				eulerX = (transform.rotation.eulerAngles.x + Y) % 360;
				eulerY = (transform.rotation.eulerAngles.y + X) % 360;
				transform.rotation = Quaternion.Euler(eulerX, eulerY, 0);
			}
			else {
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
			}

			//Keyboard commands
			Vector3 p = GetBaseInput();
			if(Input.GetKey(KeyCode.LeftShift)) {
				totalRun += Time.deltaTime;
				p = p * totalRun * shiftAdd;
				p.x = Mathf.Clamp(p.x, -maxShift, maxShift);
				p.y = Mathf.Clamp(p.y, -maxShift, maxShift);
				p.z = Mathf.Clamp(p.z, -maxShift, maxShift);
			}
			else {
				totalRun = Mathf.Clamp(totalRun * 0.5f, 1f, 1000f);
				p = p * mainSpeed;
			}

			p = p * Time.deltaTime;
			Vector3 newPosition = transform.position;
			if(Input.GetKey(KeyCode.Space)) { //If player wants to move on X and Z axis only
				transform.Translate(p);
				newPosition.x = transform.position.x;
				newPosition.z = transform.position.z;
				transform.position = newPosition;
			}
			else {
				transform.Translate(p);
			}

		}

		private Vector3 GetBaseInput() { //returns the basic values, if it's 0 than it's not active.
			Vector3 p_Velocity = new Vector3();
			if(Input.GetKey(KeyCode.W)) {
				p_Velocity += new Vector3(0, 0, 1);
			}
			if(Input.GetKey(KeyCode.S)) {
				p_Velocity += new Vector3(0, 0, -1);
			}
			if(Input.GetKey(KeyCode.A)) {
				p_Velocity += new Vector3(-1, 0, 0);
			}
			if(Input.GetKey(KeyCode.D)) {
				p_Velocity += new Vector3(1, 0, 0);
			}
			return p_Velocity;
		}
	}
}