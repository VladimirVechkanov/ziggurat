﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ziggurat {
	public class UIComponent : MonoBehaviour {
		private bool _parametrsPanelOpen = false;
		private bool _isMovePanelUnitParametrs = false;
		private bool _isMovePanelStatistics = false;
		private bool _isMovePanelManagement = false;
		public static bool _healthIsHide = false;
		[SerializeField]
		private Camera _camera;
		private UnitParametrs _unitParametrs;
		[SerializeField]
		private RectTransform _panelUnitParametrs, _panelUnitStatistics, _panelUnitManagement;
		[SerializeField]
		private float _animationSpeed = 10f;
		[SerializeField]
		private GateComponent _blueGateComponent, _redGateComponent, _greenGateComponent;
		[SerializeField]
		private Text _blueLive, _blueDie, _blueTimeToSpawn, _redLive, _redDie, _redTimeToSpawn, _greenLive, _greenDie, _greenTimeToSpawn;
		[SerializeField]
		private GameObject _canvasHealth;

		private void Update() {
			UpdateStatistics();

			if(Input.GetKeyDown(KeyCode.Mouse0) && !_isMovePanelUnitParametrs) {
				var ray = _camera.ScreenPointToRay(Input.mousePosition);

				if(Physics.Raycast(ray, out var hit)) {
					if(hit.transform.name.Contains("Gate")) {
						if(!_parametrsPanelOpen) {
							OpenClosePanelUnitParametrs_Method();
							_unitParametrs = hit.transform.GetComponent<UnitParametrs>();
							SetValuesPanelUnitParametrs();
						}
						else {
							_unitParametrs = hit.transform.GetComponent<UnitParametrs>();
							SetValuesPanelUnitParametrs();
						}
						_unitParametrs = hit.transform.GetComponent<UnitParametrs>();
					}
				}
			}
		}

		private void UpdateStatistics() {
			_blueLive.text = _blueGateComponent.Bots.Count.ToString();
			_blueDie.text = _blueGateComponent.DieCount.ToString();
			_blueTimeToSpawn.text = _blueGateComponent.Interval.ToString("0.00");
			_redLive.text = _redGateComponent.Bots.Count.ToString();
			_redDie.text = _redGateComponent.DieCount.ToString();
			_redTimeToSpawn.text = _redGateComponent.Interval.ToString("0.00");
			_greenLive.text = _greenGateComponent.Bots.Count.ToString();
			_greenDie.text = _greenGateComponent.DieCount.ToString();
			_greenTimeToSpawn.text = _greenGateComponent.Interval.ToString("0.00");
		}

		public void OpenHideHealth() {
			foreach(var bot in _blueGateComponent.Bots) {
				bot.transform.Find("Canvas").gameObject.SetActive(_healthIsHide);
			}
			foreach(var bot in _redGateComponent.Bots) {
				bot.transform.Find("Canvas").gameObject.SetActive(_healthIsHide);
			}
			foreach(var bot in _greenGateComponent.Bots) {
				bot.transform.Find("Canvas").gameObject.SetActive(_healthIsHide);
			}
			_healthIsHide = !_healthIsHide;
		}

		public void OpenClosePanelStatistics_Method() {
			if(!_isMovePanelStatistics) StartCoroutine(OpenClosePanelStatistics());
		}

		private IEnumerator OpenClosePanelStatistics() {
			_isMovePanelStatistics = true;
			ChangeInteractablePanelStatistics();

			var targetX = -_panelUnitStatistics.anchoredPosition.x;

			while(_panelUnitStatistics.anchoredPosition.x != targetX) {
				_panelUnitStatistics.anchoredPosition = Vector2.Lerp(_panelUnitStatistics.anchoredPosition, new Vector2(targetX, _panelUnitStatistics.anchoredPosition.y), _animationSpeed * Time.deltaTime);

				var roundX = Mathf.Round(_panelUnitStatistics.anchoredPosition.x * 10.0f) * 0.1f;
				if(roundX == targetX) {
					_panelUnitStatistics.anchoredPosition = new Vector2(targetX, _panelUnitStatistics.anchoredPosition.y);
				}

				if(_panelUnitStatistics.anchoredPosition.x == targetX && _isMovePanelStatistics) {
					_isMovePanelStatistics = false;
					ChangeInteractablePanelStatistics();
				}

				yield return null;
			}
		}

		private void ChangeInteractablePanelStatistics() {
			for(int i = 0; i < _panelUnitStatistics.childCount; i++) {
				var child = _panelUnitStatistics.GetChild(i);

				var button = child.GetComponent<Button>();
				if(button != null) {
					button.interactable = !_isMovePanelStatistics;
					continue;
				}
			}
		}

		public void OpenClosePanelManagement_Method() {
			if(!_isMovePanelManagement) StartCoroutine(OpenClosePanelManagement());
		}

		private IEnumerator OpenClosePanelManagement() {
			_isMovePanelManagement = true;
			ChangeInteractablePanelManagement();

			var targetX = -_panelUnitManagement.anchoredPosition.x;

			while(_panelUnitManagement.anchoredPosition.x != targetX) {
				_panelUnitManagement.anchoredPosition = Vector2.Lerp(_panelUnitManagement.anchoredPosition, new Vector2(targetX, _panelUnitManagement.anchoredPosition.y), _animationSpeed * Time.deltaTime);

				var roundX = Mathf.Round(_panelUnitManagement.anchoredPosition.x * 10.0f) * 0.1f;
				if(roundX == targetX) {
					_panelUnitManagement.anchoredPosition = new Vector2(targetX, _panelUnitManagement.anchoredPosition.y);
				}

				if(_panelUnitManagement.anchoredPosition.x == targetX && _isMovePanelManagement) {
					_isMovePanelManagement = false;
					ChangeInteractablePanelManagement();
				}

				yield return null;
			}
		}

		private void ChangeInteractablePanelManagement() {
			for(int i = 0; i < _panelUnitManagement.childCount; i++) {
				var child = _panelUnitManagement.GetChild(i);

				var button = child.GetComponent<Button>();
				if(button != null) {
					button.interactable = !_isMovePanelManagement;
					continue;
				}
			}
		}

		public void OpenClosePanelUnitParametrs_Method() {
			StartCoroutine(OpenClosePanelUnitParametrs());
		}

		private IEnumerator OpenClosePanelUnitParametrs() {
			_isMovePanelUnitParametrs = true;
			ChangeInteractablePanelUnitParametrs();

			var targetY = -_panelUnitParametrs.anchoredPosition.y;

			_parametrsPanelOpen = !_parametrsPanelOpen;

			while(_panelUnitParametrs.anchoredPosition.y != targetY) {
				_panelUnitParametrs.anchoredPosition = Vector2.Lerp(_panelUnitParametrs.anchoredPosition, new Vector2(_panelUnitParametrs.anchoredPosition.x, targetY), _animationSpeed * Time.deltaTime);

				var roundY = Mathf.Round(_panelUnitParametrs.anchoredPosition.y * 10.0f) * 0.1f;
				if(roundY == targetY) {
					_panelUnitParametrs.anchoredPosition = new Vector2(_panelUnitParametrs.anchoredPosition.x, targetY);
				}

				if(_panelUnitParametrs.anchoredPosition.y == targetY && _isMovePanelUnitParametrs) {
					_isMovePanelUnitParametrs = false;
					ChangeInteractablePanelUnitParametrs();
				}

				yield return null;
			}
		}

		private void ChangeInteractablePanelUnitParametrs() {
			for(int i = 0; i < _panelUnitParametrs.childCount; i++) {
				var child = _panelUnitParametrs.GetChild(i);

				var button = child.GetComponent<Button>();
				if(button != null) {
					button.interactable = !_isMovePanelUnitParametrs;
					continue;
				}

				var inputField = child.GetComponent<InputField>();
				if(inputField != null) {
					inputField.interactable = !_isMovePanelUnitParametrs;
					continue;
				}

				var slider = child.GetComponent<Slider>();
				if(slider != null) {
					slider.interactable = !_isMovePanelUnitParametrs;
					continue;
				}
			}
		}

		private void SetValuesPanelUnitParametrs() {
			if(_unitParametrs == null) return;

			for(int i = 0; i < _panelUnitParametrs.childCount; i++) {
				var child = _panelUnitParametrs.GetChild(i);

				var inputField = child.GetComponent<InputField>();
				if(inputField != null) {
					switch(inputField.name) {
						case "HealthCount":
							inputField.text = _unitParametrs.Health.ToString();
							break;
						case "Speed":
							inputField.text = _unitParametrs.MaxSpeed.ToString();
							break;
						case "FastDamage":
							inputField.text = _unitParametrs.FastAttackDamage.ToString();
							break;
						case "StrongDamage":
							inputField.text = _unitParametrs.StrongAttackDamage.ToString();
							break;
					}

					continue;
				}

				var slider = child.GetComponent<Slider>();
				if(slider != null) {
					switch(slider.name) {
						case "SpawnTime":
							slider.value = _unitParametrs.SpawnInterval;
							break;
						case "ProbabilityOfMiss":
							slider.value = _unitParametrs.ProbabilityOfMiss;
							break;
						case "ProbabilityOfCritical":
							slider.value = _unitParametrs.ProbabilityOfCritical;
							break;
						case "ProbabilityOfStrongAttack":
							slider.value = _unitParametrs.ProbabilityOfStrongAttack;
							break;
					}

					var value = child.Find("Value");
					if(value != null) {
						var valueText = value.GetComponent<Text>();
						valueText.text = slider.value.ToString();
					}

					continue;
				}
			}
		}

		public void OnEndEditHealthCount(string value) {
			if(value == "") return;

			_unitParametrs.Health = float.Parse(value);
		}

		public void OnEndEditSpeed(string value) {
			if(value == "") return;

			_unitParametrs.MaxSpeed = float.Parse(value);
		}

		public void OnEndEditFastDamage(string value) {
			if(value == "") return;

			_unitParametrs.FastAttackDamage = float.Parse(value);
		}

		public void OnEndEditStrongDamage(string value) {
			if(value == "") return;

			_unitParametrs.StrongAttackDamage = float.Parse(value);
		}

		public void OnChangetValueSpawnTime(float value) {
			_unitParametrs.SpawnInterval = value;

			_panelUnitParametrs.Find("SpawnTime").Find("Value").GetComponent<Text>().text = value.ToString();
		}

		public void OnChangetValueProbabilityOfMiss(float value) {
			_unitParametrs.ProbabilityOfMiss = value;

			_panelUnitParametrs.Find("ProbabilityOfMiss").Find("Value").GetComponent<Text>().text = value.ToString();
		}

		public void OnChangetValueProbabilityOfCritical(float value) {
			_unitParametrs.ProbabilityOfCritical = value;

			_panelUnitParametrs.Find("ProbabilityOfCritical").Find("Value").GetComponent<Text>().text = value.ToString();
		}

		public void OnChangetValueProbabilityOfStrongAttack(float value) {
			_unitParametrs.ProbabilityOfStrongAttack = value;

			_panelUnitParametrs.Find("ProbabilityOfStrongAttack").Find("Value").GetComponent<Text>().text = value.ToString();
		}

		public void DieAll() {
			foreach(var bot in _blueGateComponent.Bots) {
				bot.TakeDamage(float.MaxValue);
			}
			foreach(var bot in _redGateComponent.Bots) {
				bot.TakeDamage(float.MaxValue);
			}
			foreach(var bot in _greenGateComponent.Bots) {
				bot.TakeDamage(float.MaxValue);
			}
		}

		public void Clear() {
			_blueGateComponent.DieCount = 0;
			_redGateComponent.DieCount = 0;
			_greenGateComponent.DieCount = 0;
		}
	}
}