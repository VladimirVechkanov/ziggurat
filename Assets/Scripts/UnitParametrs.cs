﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ziggurat {
	public class UnitParametrs : MonoBehaviour {
		public float Health = 100f, MaxSpeed = 10f, FastAttackDamage = 50f, StrongAttackDamage = 90f,
			ProbabilityOfMiss = 0.1f, ProbabilityOfCritical = 0.5f, ProbabilityOfStrongAttack = 0.5f,
			SpawnInterval = 3f;
	}
}