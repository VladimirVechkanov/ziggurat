﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Ziggurat {
	public class VisibilitySphereComponent : MonoBehaviour {
		private BotComponent _parent;
		[SerializeField]
		private List<BotComponent> _enemies = new List<BotComponent>();

		private void Start() {
			_parent = GetComponentInParent<BotComponent>();

			transform.localScale = new Vector3(_parent.VisibilityDistance, _parent.VisibilityDistance, _parent.VisibilityDistance);
		}

		private void Update() {
			var dieEnemies = _enemies.Where(t => t.Health <= 0f).ToList();
			foreach(var dieEnemy in dieEnemies) {
				_enemies.Remove(dieEnemy);
			}

			if(_parent.Target != null && _parent.Target.Health <= 0f) {
				var maxDist = float.MaxValue;
				BotComponent newTarget = null;

				foreach(var enemy in _enemies) {
					var dist = Vector3.Distance(_parent.transform.position, enemy.transform.position);
					
					if(dist < maxDist) {
						maxDist = dist;
						newTarget = enemy;
					}
				}

				if(newTarget != null) {
					_parent.Target = newTarget;
				}
			}
		}

		private void OnTriggerEnter(Collider other) {
			var botComponent = other.GetComponent<BotComponent>();
			if(botComponent != null && botComponent.Color != _parent.Color && botComponent.Health > 0f && botComponent.name != "Pool") {
				_enemies.Add(botComponent);
				if(_parent.Target == null || _parent.Target.name == "Pool") {
					_parent.Target = botComponent;
				}
			}
		}
	}
}